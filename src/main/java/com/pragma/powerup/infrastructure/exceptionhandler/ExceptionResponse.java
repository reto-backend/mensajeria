package com.pragma.powerup.infrastructure.exceptionhandler;

import lombok.Getter;

@Getter
public enum ExceptionResponse {
    NO_DATA_FOUND("No data found for the requested petition"),
    API_EXCEPTION("Error while trying to send the SMS");

    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }
}