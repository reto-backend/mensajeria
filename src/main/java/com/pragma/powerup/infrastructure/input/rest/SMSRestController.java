package com.pragma.powerup.infrastructure.input.rest;

import com.pragma.powerup.application.dto.request.SMSRequestDto;
import com.pragma.powerup.infrastructure.out.twilio.SMSService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class SMSRestController {

    private final SMSService smsService;

    @PostMapping("/processSMS")
    public ResponseEntity<Map<String, String>> processSMS(@RequestBody SMSRequestDto smsRequestDto) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(Collections.singletonMap("message", smsService.sendSMS(
                        smsRequestDto.getDestinationSMSNumber(),
                        smsRequestDto.getSmsMessage()
                )));
    }
}
