package com.pragma.powerup.infrastructure.out.twilio;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@NoArgsConstructor
public class SMSService {

    @Value("${TWILIO_ACCOUNT_SID}")
    String accountSid;

    @Value("${TWILIO_AUTH_TOKEN}")
    String authToken;

    @Value("${TWILIO_OUTGOING_SMS_NUMBER}")
    String outgoingSmsNumber;

    @PostConstruct
    public void setup(){
        Twilio.init(accountSid, authToken);
    }
    public String sendSMS(String smsNumber, String smsMessage){

        Message message = Message.creator(
                new PhoneNumber(smsNumber),
                new PhoneNumber(outgoingSmsNumber),
                smsMessage)
                .create();

        return message.getStatus().toString();
    }
}
