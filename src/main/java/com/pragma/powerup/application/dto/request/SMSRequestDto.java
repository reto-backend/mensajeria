package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SMSRequestDto {
    private String destinationSMSNumber;
    private String smsMessage;
}
